package com.manman.config;

import java.util.Arrays;

import org.springdoc.core.models.GroupedOpenApi;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import io.swagger.v3.oas.models.Components;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.security.SecurityRequirement;
import io.swagger.v3.oas.models.security.SecurityScheme;

@Configuration
public class SwaggerConfig {

    @Bean
    public GroupedOpenApi manmanApi() {
        return GroupedOpenApi.builder()
                .group("manman Service API v1")
                .pathsToMatch("/**")
                .build();
    }
    
    @Bean
    public OpenAPI manmanOpenAPI(@Value("${springdoc.version}") String springdocVersion) {
    	
    	Info info = new Info().title("manman Service API 명세서")
				.description("manman 프로젝트 서비스 API 명세서입니다.")
				.version(springdocVersion);
				//.termsOfService("http://swagger.io/terms/")
				//.contact(new Contact().name("jini").url("https://blog.jiniworld.me/").email("jini@jiniworld.me"))
				//.license(new License().name("Apache License Version 2.0").url("http://www.apache.org/licenses/LICENSE-2.0"));
    	
    	SecurityScheme securityScheme = new SecurityScheme()
				.type(SecurityScheme.Type.HTTP).scheme("bearer").bearerFormat("JWT")
				.in(SecurityScheme.In.HEADER).name("Authorization");
    	
		SecurityRequirement schemaRequirement = new SecurityRequirement().addList("bearerAuth");
    	
        return new OpenAPI()
				.components(new Components().addSecuritySchemes("bearerAuth", securityScheme))
				.security(Arrays.asList(schemaRequirement))
				.info(info);
        
    }
    
}