package com.manman.config;

import java.io.IOException;

import javax.sql.DataSource;

import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@MapperScan(basePackages = "com.manman.mapper.manman", sqlSessionFactoryRef = "manmanSqlSessionFactory")
@EnableTransactionManagement
public class ManmanDatabaseConfig {
	
	@Primary
	@Bean(name = "manmanDataSource")
	@ConfigurationProperties(prefix = "spring.manman.datasource")
	public DataSource manmanDataSource() {
		return DataSourceBuilder.create().build();
	}

	@Primary
	@Bean(name = "manmanSqlSessionFactory")
	public SqlSessionFactory sqlSessionFactory(@Qualifier("manmanDataSource") DataSource manmanDataSource)
			throws IOException, Exception {
		SqlSessionFactoryBean sqlSessionFactory = new SqlSessionFactoryBean();
		sqlSessionFactory.setDataSource(manmanDataSource);
		sqlSessionFactory.setMapperLocations(
				new PathMatchingResourcePatternResolver().getResources("classpath:/mappers/manman/*.xml"));
		Resource myBatisConfig = new PathMatchingResourcePatternResolver().getResource("classpath:mybatis-config.xml");
		sqlSessionFactory.setConfigLocation(myBatisConfig);
		return sqlSessionFactory.getObject();
	}

	@Primary
	@Bean(name = "manmanSqlSessionTemplate")
	public SqlSessionTemplate sqlSession(@Qualifier("manmanSqlSessionFactory") SqlSessionFactory manmanSqlSessionFactory) {
		
		return new SqlSessionTemplate(manmanSqlSessionFactory);
	}

	/**
	 * 트랜잭션 매니저 설정
	 */
	@Primary
	@Bean(name = "manmanTransactionManager")
	public PlatformTransactionManager transactionManager(@Qualifier("manmanDataSource") DataSource manmanDataSource) {
		return new DataSourceTransactionManager(manmanDataSource);
	}
}
