package com.manman.mapper.manman;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import com.manman.lostarticle.foundarticle.vo.FoundArticle;
import com.manman.lostarticle.foundarticle.vo.FoundArticleRequest;

@Mapper
@Repository
public interface FoundArticleMapper {
	
	public List<FoundArticle> selectFoundArticleList(FoundArticleRequest foundArticleRequest) throws Exception;
	
	public int selectCountFoundArticleList(FoundArticleRequest foundArticleRequest) throws Exception;

}
