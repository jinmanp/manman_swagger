package com.manman.mapper.manman;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import com.manman.music.vo.MusicRequest;
import com.manman.music.vo.MusicResponse;

@Mapper
@Repository
public interface MusicMapper {
	
	public List<MusicResponse> selectRMusicList(MusicRequest musicRequest) throws Exception;
	
	public List<MusicResponse> selectD01MusicList(MusicRequest musicRequest) throws Exception;
	
	public List<MusicResponse> selectD02MusicList(MusicRequest musicRequest) throws Exception;
	
	public List<MusicResponse> selectYMusicList(MusicRequest musicRequest) throws Exception;

	public int updateTop100Realtime(MusicRequest musicRequest) throws Exception;
	
	public int updateTop100Day(MusicRequest musicRequest) throws Exception;
	
	public int updateTop100Year(MusicRequest musicRequest) throws Exception;

}
