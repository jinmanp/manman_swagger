package com.manman.mapper.manman;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import com.manman.lostarticle.lostarticle.vo.LostArticle;
import com.manman.lostarticle.lostarticle.vo.LostArticleRequest;

@Mapper
@Repository
public interface LostArticleMapper {
	
	public List<LostArticle> selectLostArticleList(LostArticleRequest lostArticleRequest) throws Exception;
	
	public int selectCountLostArticleList(LostArticleRequest lostArticleRequest) throws Exception;

}
