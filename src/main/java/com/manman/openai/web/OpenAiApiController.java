package com.manman.openai.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.manman.common.response.ErrorResponse;
import com.manman.lostarticle.foundarticle.vo.FoundArticleResponse;
import com.manman.openai.service.OpenAiApiService;
import com.manman.openai.vo.ChatRequest;
import com.manman.openai.vo.OpenAiRequest;
import com.manman.openai.vo.OpenAiResponse;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.parameters.RequestBody;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;

@Tag(name = "OpenAi", description = "OpenAi api 입니다.")
@RestController
@RequestMapping("/openAi")
public class OpenAiApiController {
	
	final private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
    private OpenAiApiService openAiApiService;
	
	@Operation(summary = "openAi qna", description = "openAi qna")
    @ApiResponses({
            @ApiResponse(responseCode = "200", description = "OK",
                    content = @Content(schema = @Schema(implementation = FoundArticleResponse.class))),
            @ApiResponse(responseCode = "400", description = "BAD REQUEST", 
            		content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = "404", description = "NOT FOUND",
            		content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = "500", description = "INTERNAL SERVER ERROR",
            		content = @Content(schema = @Schema(implementation = ErrorResponse.class)))
    })
	@Parameters({
		@Parameter(name = "user", description = "user", required = false),
		@Parameter(name = "prompt", description = "prompt", required = true),
		@Parameter(name = "model", description = "model", example = "2023-02-28", required = true),
		@Parameter(name = "maxTokens", description = "maxTokens", required = true),
		@Parameter(name = "topP", description = "topP", required = true)
	})
	@RequestMapping(value = "/qna", method = {RequestMethod.POST, RequestMethod.GET})
	ResponseEntity<OpenAiResponse> requestQnaApi(
			@Parameter(hidden = true) @Validated(OpenAiRequest.validSelectQna.class) @RequestBody OpenAiRequest openAiRequest
    		) throws Exception {
		
		logger.debug("OpenAiApiController requestQnaApi openAiRequest.getUser() : " + openAiRequest.getUser());
		logger.debug("OpenAiApiController requestQnaApi openAiRequest.getPrompt() : " + openAiRequest.getPrompt());
		logger.debug("OpenAiApiController requestQnaApi openAiRequest.getModel() : " + openAiRequest.getModel());
		logger.debug("OpenAiApiController requestQnaApi openAiRequest.getMaxTokens() : " + openAiRequest.getMaxTokens());
		logger.debug("OpenAiApiController requestQnaApi openAiRequest.getTopP() : " + openAiRequest.getTopP());
		
		OpenAiResponse openAiResponse = openAiApiService.requestQnaApi(openAiRequest);
		
		return ResponseEntity.ok().body(openAiResponse);
		
	}
	
	@Operation(summary = "openAi chat", description = "openAi chat")
    @ApiResponses({
            @ApiResponse(responseCode = "200", description = "OK",
                    content = @Content(schema = @Schema(implementation = FoundArticleResponse.class))),
            @ApiResponse(responseCode = "400", description = "BAD REQUEST", 
            		content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = "404", description = "NOT FOUND",
            		content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = "500", description = "INTERNAL SERVER ERROR",
            		content = @Content(schema = @Schema(implementation = ErrorResponse.class)))
    })
	@Parameters({
		@Parameter(name = "user", description = "user", required = false),
		@Parameter(name = "prompt", description = "prompt", required = true),
		@Parameter(name = "model", description = "model", example = "2023-02-28", required = true),
		@Parameter(name = "maxTokens", description = "maxTokens", required = true),
		@Parameter(name = "topP", description = "topP", required = true)
	})
	@RequestMapping(value = "/chat", method = {RequestMethod.POST, RequestMethod.GET})
	ResponseEntity<OpenAiResponse> requestChatCompletionApi(
			@Parameter(hidden = true) @Validated(ChatRequest.validSelectChat.class) @RequestBody ChatRequest chatRequest
    		) throws Exception {
		
		logger.debug("OpenAiApiController requestChatCompletionApi chatRequest.getUser() : " + chatRequest.getUser());
		logger.debug("OpenAiApiController requestChatCompletionApi chatRequest.getModel() : " + chatRequest.getModel());
		logger.debug("OpenAiApiController requestChatCompletionApi chatRequest.getMessages() : " + chatRequest.getMessages());
		logger.debug("OpenAiApiController requestChatCompletionApi chatRequest.getMaxTokens() : " + chatRequest.getMaxTokens());
		logger.debug("OpenAiApiController requestChatCompletionApi chatRequest.getTopP() : " + chatRequest.getTopP());
		
		OpenAiResponse openAiResponse = openAiApiService.requestChatCompletionApi(chatRequest);
		
		return ResponseEntity.ok().body(openAiResponse);
		
	}
	
}
