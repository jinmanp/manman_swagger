package com.manman.openai.vo;

import java.util.List;
import java.util.Map;

import com.theokanning.openai.completion.chat.ChatMessage;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ChatRequest {
	
	public interface validSelectChat {}
	
	@NotBlank(groups = validSelectChat.class)
	@Schema(description = "모델 ID")
	private String model;
	
	@NotBlank(groups = validSelectChat.class)
	@Schema(description = "메시지")
	private List<ChatMessage> messages;
	
	// default는 1.0 이며 0~2 사이 설정. 0에 가까울수록 정제된 문장, 높은  에 가까울수록 창의적이고 random한 문장
	private int temperature;
	
	// 모델이 사용할 토큰에 대한 threshold 라고 보면 된다.  temperature 의 대안. 둘 중 하나만 조정하기를 추천.
	private int topP;
	
	private int n;
	
	private boolean stream;
	
	@Schema(description = "중지 문자열")
	private String[] stop;
	
	// 대부분의 모델들은 최대 2048 token 까지 지원하나, 최신 모델은 4096 토큰까지 지원 가능
	@Schema(description = "(요청+응답) 텍스트 최대 길이")
	private int maxTokens;
	
	// 특정 단어나 phrase 를 포함하지 않도록. -2~2 까지 조정 가능한테, 2에 가까울수록 penalty 가 커진다
	private int presencePenalty;
	
	// 반복적이지 않은 텍스트를 생성하도록 유도. 반복되며 penalty 부여되며 2에 가까울수록 penalty 가 커진다
	private int frequencyPenalty;
	
	private Map<String, Object> logitBias;
	
	private String user;
	
}
