package com.manman.openai.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OpenAiRequest {
	
	public interface validSelectQna {}
	
	@Schema(description = "사용자")
	private String user;
	
	@Schema(description = "등록일자")
	private String prompt;
	
	@NotBlank(groups = validSelectQna.class)
	@Schema(description = "모델 ID")
	private String model;
	
	@Schema(description = "메시지")
	private String messages;
	
	@Schema(description = "시작일자")
	private String maxTokens;
	
	@Schema(description = "종료일자")
	private String topP;

}
