package com.manman.openai.service;

import com.manman.openai.vo.ChatRequest;
import com.manman.openai.vo.OpenAiRequest;
import com.manman.openai.vo.OpenAiResponse;

public interface OpenAiApiService {

	OpenAiResponse requestQnaApi(OpenAiRequest openAiRequest) throws Exception;

	OpenAiResponse requestChatCompletionApi(ChatRequest chatRequest) throws Exception;

}
