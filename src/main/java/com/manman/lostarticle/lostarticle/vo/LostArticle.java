package com.manman.lostarticle.lostarticle.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Schema(description = "분실물", hidden = true)
public class LostArticle {
	
	@Schema(description = "관리ID")
	private String atcId;
	@Schema(description = "분실물 이미지 경로")
	private String lstFilePathImg;
	@Schema(description = "분실시간")
	private String lstHor;
	@Schema(description = "분실지역명")
	private String lstLctNm;
	@Schema(description = "분실지역코드")
	private String lstLctCd;
	@Schema(description = "분실장소")
	private String lstPlace;
	@Schema(description = "분실장소구분")
	private String lstPlaceSeNm;
	@Schema(description = "물품명")
	private String lstPrdtNm;
	@Schema(description = "제목")
	private String lstSbjt;
	@Schema(description = "분실물 상태")
	private String lstSteNm;
	@Schema(description = "분실일자")
	private String lstYmd;
	@Schema(description = "기관ID")
	private String orgId;
	@Schema(description = "기관명")
	private String orgNm;
	@Schema(description = "물품 분류")
	private String prdtClNm;
	@Schema(description = "대분류 코드")
	private String prdtClCd01;
	@Schema(description = "중분류 코드")
	private String prdtClCd02;
	@Schema(description = "기관 전화번호")
	private String tel;
	@Schema(description = "특이사항")
	private String uniq;
	@Schema(description = "등록일")
	private String regYmd;

}
