package com.manman.lostarticle.lostarticle.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.manman.lostarticle.lostarticle.service.LostArticleService;
import com.manman.lostarticle.lostarticle.vo.LostArticle;
import com.manman.lostarticle.lostarticle.vo.LostArticleRequest;
import com.manman.lostarticle.lostarticle.vo.LostArticleResponse;
import com.manman.mapper.manman.LostArticleMapper;

/**
 * LostArticle ServiceImpl 클래스
 * @author 박진만
 * @since 2019.04.15
 * @version 1.0.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *  수정일			수정자			수정내용
 *  -------    		--------    ---------------------------
 *  2019.04.15		박진만 		최초 생성
 * </pre>
 */
@Service("LostArticleService")
@Transactional(propagation=Propagation.SUPPORTS, readOnly=true)
public class LostArticleServiceImpl implements LostArticleService{
	
	final private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private LostArticleMapper lostArticleMapper;

	/**
	 * 분실물 리스트 조회
	 */
	@Override
	public List<LostArticle> selectLostArticleList(LostArticleRequest lostArticleRequest) throws Exception {
		return lostArticleMapper.selectLostArticleList(lostArticleRequest);
	}
	
	@Override
	public int selectCountLostArticleList(LostArticleRequest lostArticleRequest) throws Exception {
		return lostArticleMapper.selectCountLostArticleList(lostArticleRequest);
	}

	@Override
	public LostArticleResponse selectLostArticle(LostArticleRequest lostArticleRequest) throws Exception {
		
		int pageSize = lostArticleRequest.getPageSize();// 한페이지에 나오는 게시물 개수
		int pageNo = lostArticleRequest.getPageNo(); //현재 선택한 페이지 number
		int startRow = (pageNo - 1) * pageSize + 1;// 한 페이지의 시작글 번호
		int endRow = pageNo * pageSize;// 한 페이지의 마지막 글번호

		lostArticleRequest.setStartRow(startRow);
		lostArticleRequest.setEndRow(endRow);
		
		int totalCount = lostArticleMapper.selectCountLostArticleList(lostArticleRequest); //게시물 총 개수
		
		List<LostArticle> resultList = lostArticleMapper.selectLostArticleList(lostArticleRequest);
		
		LostArticleResponse lostArticleResponse = LostArticleResponse.builder()
				.pageNo(lostArticleRequest.getPageNo())
				.pageSize(lostArticleRequest.getPageSize())
				.totalCount(totalCount)
				.resultList(resultList)
				.build();
		
		return lostArticleResponse;
	}
	
}
