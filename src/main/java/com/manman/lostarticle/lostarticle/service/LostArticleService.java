package com.manman.lostarticle.lostarticle.service;

import java.util.List;

import com.manman.lostarticle.lostarticle.vo.LostArticle;
import com.manman.lostarticle.lostarticle.vo.LostArticleRequest;
import com.manman.lostarticle.lostarticle.vo.LostArticleResponse;

/**
 * LostArticle Servicel 클래스
 * @author 박진만
 * @since 2019.04.15
 * @version 1.0.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *  수정일			수정자			수정내용
 *  -------    		--------    ---------------------------
 *  2019.04.15		박진만 		최초 생성
 * </pre>
 */
public interface LostArticleService {	

	/**
	 * 분실물 리스트 조회
	 */
	List<LostArticle> selectLostArticleList(LostArticleRequest lostArticleRequest) throws Exception;
	
	int selectCountLostArticleList(LostArticleRequest lostArticleRequest) throws Exception;

	LostArticleResponse selectLostArticle(LostArticleRequest lostArticleRequest) throws Exception;

}
