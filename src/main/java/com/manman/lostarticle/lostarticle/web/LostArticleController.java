package com.manman.lostarticle.lostarticle.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.manman.common.response.ErrorResponse;
import com.manman.lostarticle.foundarticle.vo.FoundArticleResponse;
import com.manman.lostarticle.lostarticle.service.LostArticleService;
import com.manman.lostarticle.lostarticle.vo.LostArticleRequest;
import com.manman.lostarticle.lostarticle.vo.LostArticleResponse;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.parameters.RequestBody;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;

@Tag(name = "LostArticle", description = "LostArticle api 입니다.")
@RestController
@RequestMapping("/lostArticle")
public class LostArticleController {
	
	final private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
    private LostArticleService lostArticleService;
	
	@Operation(summary = "분실물 리스트 조회", description = "분실물 리스트 정보가 조회됩니다.")
    @ApiResponses({
            @ApiResponse(responseCode = "200", description = "OK",
                    content = @Content(schema = @Schema(implementation = FoundArticleResponse.class))),
            @ApiResponse(responseCode = "400", description = "BAD REQUEST", 
            		content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = "404", description = "NOT FOUND",
            		content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = "500", description = "INTERNAL SERVER ERROR",
            		content = @Content(schema = @Schema(implementation = ErrorResponse.class)))
    })
	@Parameters({
		@Parameter(name = "pageNo", description = "페이지 번호", example = "1", required = false),
		@Parameter(name = "pageSize", description = "페이지 사이즈", example = "20", required = false),
		@Parameter(name = "regYmd", description = "등록일자", example = "2023-02-28", required = false),
		@Parameter(name = "searchRegYmd", description = "등록일자 기준 조회", example = "false", schema = @Schema(type = "boolean", allowableValues = {"true", "false"}), required = false),
		@Parameter(name = "startYmd", description = "시작일자", example = "", required = false),
		@Parameter(name = "endYmd", description = "종료일자", example = "", required = false),
		@Parameter(name = "lstLctCd", description = "분실지역코드", example = "", required = false),
		@Parameter(name = "prdtClCd01", description = "대분류 코드", example = "", required = false),
		@Parameter(name = "prdtClCd02", description = "중분류 코드", example = "", required = false),
		@Parameter(name = "searchNm", description = "물품명", example = "", required = false)
	})
	@RequestMapping(value = "/selectLostArticleList", method = {RequestMethod.POST, RequestMethod.GET})
	ResponseEntity<LostArticleResponse> selectLostArticleList(
			@Parameter(hidden = true) @Validated(LostArticleRequest.validSelectLostArticleList.class) @RequestBody LostArticleRequest lostArticleRequest
    		) throws Exception {
		
		logger.debug("LostArticleController selectLostArticleList lostArticleRequest.getPageNo() : " + lostArticleRequest.getPageNo());
		logger.debug("LostArticleController selectLostArticleList lostArticleRequest.getPageSize() : " + lostArticleRequest.getPageSize());
		logger.debug("LostArticleController selectLostArticleList lostArticleRequest.getStartYmd() : " + lostArticleRequest.getStartYmd());
		logger.debug("LostArticleController selectLostArticleList lostArticleRequest.getEndYmd() : " + lostArticleRequest.getEndYmd());
		logger.debug("LostArticleController selectLostArticleList lostArticleRequest.getSearchNm() : " + lostArticleRequest.getSearchNm());
		logger.debug("LostArticleController selectLostArticleList lostArticleRequest.getSearchRegYmd() : " + lostArticleRequest.getSearchRegYmd());
		
		LostArticleResponse lostArticleResponse = lostArticleService.selectLostArticle(lostArticleRequest);
		
		return ResponseEntity.ok().body(lostArticleResponse);
		
	}
	
}
