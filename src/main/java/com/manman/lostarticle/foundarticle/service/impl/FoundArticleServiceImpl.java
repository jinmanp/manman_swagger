package com.manman.lostarticle.foundarticle.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.manman.lostarticle.foundarticle.service.FoundArticleService;
import com.manman.lostarticle.foundarticle.vo.FoundArticle;
import com.manman.lostarticle.foundarticle.vo.FoundArticleRequest;
import com.manman.lostarticle.foundarticle.vo.FoundArticleResponse;
import com.manman.mapper.manman.FoundArticleMapper;

/**
 * FoundArticle ServiceImpl 클래스
 * @author 박진만
 * @since 2019.04.18
 * @version 1.0.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *  수정일			수정자			수정내용
 *  -------    		--------    ---------------------------
 *  2019.04.18		박진만 		최초 생성
 * </pre>
 */
@Service("FoundArticleService")
@Transactional(propagation=Propagation.SUPPORTS, readOnly=true)
public class FoundArticleServiceImpl implements FoundArticleService{
	
	final private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private FoundArticleMapper foundArticleMapper;

	/**
	 * 습득물 리스트 조회
	 */
	@Override
	public List<FoundArticle> selectFoundArticleList(FoundArticleRequest foundArticleRequest) throws Exception {
		return foundArticleMapper.selectFoundArticleList(foundArticleRequest);
	}
	
	@Override
	public int selectCountFoundArticleList(FoundArticleRequest foundArticleRequest) throws Exception {
		return foundArticleMapper.selectCountFoundArticleList(foundArticleRequest);
	}

	@Override
	public FoundArticleResponse selectFoundArticle(FoundArticleRequest foundArticleRequest) throws Exception {
		
		int pageSize = foundArticleRequest.getPageSize();// 한페이지에 나오는 게시물 개수
		int pageNo = foundArticleRequest.getPageNo(); //현재 선택한 페이지 number
		int startRow = (pageNo - 1) * pageSize + 1;// 한 페이지의 시작글 번호
		int endRow = pageNo * pageSize;// 한 페이지의 마지막 글번호

		foundArticleRequest.setStartRow(startRow);
		foundArticleRequest.setEndRow(endRow);
		
		int totalCount = foundArticleMapper.selectCountFoundArticleList(foundArticleRequest); //게시물 총 개수
				
		List<FoundArticle> resultList = foundArticleMapper.selectFoundArticleList(foundArticleRequest);
		
		FoundArticleResponse foundArticleResponse = FoundArticleResponse.builder()
				.pageNo(foundArticleRequest.getPageNo())
				.pageSize(foundArticleRequest.getPageSize())
				.totalCount(totalCount)
				.resultList(resultList)
				.build();
		
		return foundArticleResponse;
	}
	
}
