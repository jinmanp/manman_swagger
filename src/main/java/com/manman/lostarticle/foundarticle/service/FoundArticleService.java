package com.manman.lostarticle.foundarticle.service;

import java.util.List;

import com.manman.lostarticle.foundarticle.vo.FoundArticle;
import com.manman.lostarticle.foundarticle.vo.FoundArticleRequest;
import com.manman.lostarticle.foundarticle.vo.FoundArticleResponse;

/**
 * FoundArticle Service 클래스
 * @author 박진만
 * @since 2019.04.18
 * @version 1.0.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *  수정일			수정자			수정내용
 *  -------    		--------    ---------------------------
 *  2019.04.18		박진만 		최초 생성
 * </pre>
 */
public interface FoundArticleService {	

	/**
	 * 습득물 리스트 조회
	 */
	List<FoundArticle> selectFoundArticleList(FoundArticleRequest foundArticleRequest) throws Exception;
	
	int selectCountFoundArticleList(FoundArticleRequest foundArticleRequest) throws Exception;

	FoundArticleResponse selectFoundArticle(FoundArticleRequest foundArticleRequest) throws Exception;
	
}
