package com.manman.lostarticle.foundarticle.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Schema(description = "습득물", hidden = true)
public class FoundArticle {
	
	@Schema(description = "관리ID")
	private String atcId;
	@Schema(description = "보관상태")
	private String csteSteNm;
	@Schema(description = "보관장소")
	private String depPlace;
	@Schema(description = "습득물 이미지 경로")
	private String fdFilePathImg;
	@Schema(description = "습득시간")
	private String fdHor;
	@Schema(description = "습득지역명")
	private String fdLctNm;
	@Schema(description = "습득지역코드")
	private String fdLctCd;
	@Schema(description = "습득장소")
	private String fdPlace;
	@Schema(description = "물품명")
	private String fdPrdtNm;
	@Schema(description = "습득순번")
	private String fdSn;
	@Schema(description = "습득일자")
	private String fdYmd;
	@Schema(description = "습득물보관구분")
	private String fndKeepOrgn;
	@Schema(description = "기관ID")
	private String orgId;
	@Schema(description = "기관명")
	private String orgNm;
	@Schema(description = "물품 분류")
	private String prdtClNm;
	@Schema(description = "대분류 코드")
	private String prdtClCd01;
	@Schema(description = "중분류 코드")
	private String prdtClCd02;
	@Schema(description = "기관 전화번호")
	private String tel;
	@Schema(description = "특이사항")
	private String uniq;
	@Schema(description = "등록일")
	private String regYmd;

}
