package com.manman.lostarticle.foundarticle.vo;

import java.util.List;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class FoundArticleResponse {
	
	@Schema(description = "페이지 번호")
    private int pageNo;
	@Schema(description = "페이지 사이즈")
    private int pageSize;
	@Schema(description = "전체 건수")
	private int totalCount;
	
	@Schema(description = "습득물 목록")
	private List<FoundArticle> resultList;
	
}
