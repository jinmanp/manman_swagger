package com.manman.lostarticle.foundarticle.vo;

import com.manman.lostarticle.lostarticle.vo.LostArticleRequest.validSelectLostArticleList;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FoundArticleRequest {
	
	public interface validSelectFoundArticleList {}
	
	@Schema(description = "한 페이지의 시작글 번호", hidden = true)
	private int startRow;
	@Schema(description = "한 페이지의 마지막 글번호", hidden = true)
	private int endRow;
	
	@NotNull(groups = validSelectFoundArticleList.class)
	@Positive(groups = validSelectFoundArticleList.class)
	//@Min(value = 1, groups = validSelectFoundArticleList.class)
	@Schema(description = "페이지 번호", defaultValue = "1")
    private int pageNo = 1;
	
	@NotNull(groups = validSelectFoundArticleList.class)
	@Schema(description = "페이지 사이즈", defaultValue = "20")
    private int pageSize = 20; //한페이지에 나오는 게시물 개수
	
	@Schema(description = "등록일자")
	private String regYmd;
	
	@Schema(description = "등록일자 기준 조회")
	private Boolean searchRegYmd;
	
	@Schema(description = "시작일자")
	private String startYmd;
	
	@Schema(description = "종료일자")
	private String endYmd;
	
	@Schema(description = "습득지역코드")
	private String fdLctCd;
	
	@Schema(description = "대분류 코드")
	private String prdtClCd01;
	
	@Schema(description = "중분류 코드")
	private String prdtClCd02;
	
	@Schema(description = "물품명")
	private String searchNm;
	
}
