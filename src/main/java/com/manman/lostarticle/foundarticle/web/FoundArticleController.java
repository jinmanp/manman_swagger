package com.manman.lostarticle.foundarticle.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.manman.common.response.ErrorResponse;
import com.manman.lostarticle.foundarticle.service.FoundArticleService;
import com.manman.lostarticle.foundarticle.vo.FoundArticleRequest;
import com.manman.lostarticle.foundarticle.vo.FoundArticleResponse;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.parameters.RequestBody;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;

@Tag(name = "FoundArticle", description = "FoundArticle api 입니다.")
@RestController
@RequestMapping("/foundArticle")
public class FoundArticleController {
	
	final private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
    private FoundArticleService foundArticleService;
	
	@Operation(summary = "습득물 리스트 조회", description = "습득물 리스트 정보가 조회됩니다.")
    @ApiResponses({
            @ApiResponse(responseCode = "200", description = "OK",
                    content = @Content(schema = @Schema(implementation = FoundArticleResponse.class))),
            @ApiResponse(responseCode = "400", description = "BAD REQUEST", 
            		content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = "404", description = "NOT FOUND",
            		content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = "500", description = "INTERNAL SERVER ERROR",
            		content = @Content(schema = @Schema(implementation = ErrorResponse.class)))
    })
	@Parameters({
		@Parameter(name = "pageNo", description = "페이지 번호", example = "1", required = false),
		@Parameter(name = "pageSize", description = "페이지 사이즈", example = "20", required = false),
		@Parameter(name = "regYmd", description = "등록일자", required = false),
		@Parameter(name = "searchRegYmd", description = "등록일자 기준 조회", example = "false", schema = @Schema(type = "boolean", allowableValues = {"true", "false"}), required = false),
		@Parameter(name = "startYmd", description = "시작일자", example = "", required = false),
		@Parameter(name = "endYmd", description = "종료일자", example = "", required = false),
		@Parameter(name = "fdLctCd", description = "습득지역코드", example = "", required = false),
		@Parameter(name = "prdtClCd01", description = "대분류 코드", example = "", required = false),
		@Parameter(name = "prdtClCd02", description = "중분류 코드", example = "", required = false),
		@Parameter(name = "searchNm", description = "물품명", example = "", required = false)
	})
	@RequestMapping(value = "/selectFoundArticleList", method = {RequestMethod.POST, RequestMethod.GET})
	ResponseEntity<FoundArticleResponse> selectFoundArticleList(
			@Parameter(hidden = true) @Validated(FoundArticleRequest.validSelectFoundArticleList.class) @RequestBody FoundArticleRequest foundArticleRequest
    		) throws Exception {
		
		logger.debug("FoundArticleController selectFoundArticleList foundArticleRequest.getPageNo() : " + foundArticleRequest.getPageNo());
		logger.debug("FoundArticleController selectFoundArticleList foundArticleRequest.getPageSize() : " + foundArticleRequest.getPageSize());
		logger.debug("FoundArticleController selectFoundArticleList foundArticleRequest.getStartYmd() : " + foundArticleRequest.getStartYmd());
		logger.debug("FoundArticleController selectFoundArticleList foundArticleRequest.getEndYmd() : " + foundArticleRequest.getEndYmd());
		logger.debug("FoundArticleController selectFoundArticleList foundArticleRequest.getSearchNm() : " + foundArticleRequest.getSearchNm());
		logger.debug("FoundArticleController selectFoundArticleList foundArticleRequest.getSearchRegYmd() : " + foundArticleRequest.getSearchRegYmd());
		
		FoundArticleResponse foundArticleResponse = foundArticleService.selectFoundArticle(foundArticleRequest);
		
		return ResponseEntity.ok().body(foundArticleResponse);
		
    }
	
}
