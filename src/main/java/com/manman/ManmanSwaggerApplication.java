package com.manman;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ManmanSwaggerApplication {

	public static void main(String[] args) {
		SpringApplication.run(ManmanSwaggerApplication.class, args);
	}

}
