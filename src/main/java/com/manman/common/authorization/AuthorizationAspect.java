package com.manman.common.authorization;

import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.regex.Pattern;

import javax.crypto.SecretKey;

import org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.manman.common.exception.AuthorizationHeaderNotExistsException;
import com.manman.common.exception.InvalidTokenException;
import com.manman.common.exception.TokenExpiredException;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.security.Keys;
import io.jsonwebtoken.security.WeakKeyException;
import jakarta.servlet.http.HttpServletRequest;
import lombok.Setter;

@ConfigurationProperties(prefix = "manman.jwt.token")
@Aspect
@Component
public class AuthorizationAspect {
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Setter 
	private String apiKey;
	
	@Setter 
	private String secretKey;

	//@Before("execution(public * com.manman..*Controller.*(..))")
	@Before("execution(* com.manman..*Controller.*(..)) "
			+ "&& !target(com.manman.common.web.TokenController) "
			+ "&& !target(com.manman.lostarticle.lostarticle.web.LostArticleController) "
			+ "&& !target(com.manman.lostarticle.foundarticle.web.FoundArticleController)")
	public void checkToken(JoinPoint joinPoint) throws WeakKeyException, UnsupportedEncodingException, TokenExpiredException {
				
		logger.debug("AuthorizationAspect checkToken");
		
		SecretKey key = Keys.hmacShaKeyFor(secretKey.getBytes("UTF-8"));
		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
		String authorization = request.getHeader("Authorization");
		
		if(StringUtils.isBlank(authorization)){
			throw new AuthorizationHeaderNotExistsException();
		}
		
		if(Pattern.matches("^Bearer .*", authorization)) {
			
			authorization = authorization.replaceAll("^Bearer( )*", "");
			Jws<Claims> jwsClaims = Jwts.parserBuilder()
					.setSigningKey(key)
					.build()
					.parseClaimsJws(authorization);

			if(jwsClaims.getBody() != null) {
				
				Claims claims = jwsClaims.getBody();
				
				logger.debug("AuthorizationAspect checkToken claims : " + claims);
				
				if(!claims.containsKey("apiKey") || !apiKey.equals(claims.get("apiKey").toString())
						|| claims.getExpiration() == null) {
					throw new InvalidTokenException();
				}
				
				long exp = claims.getExpiration().getTime();
				
				logger.debug("AuthorizationAspect checkToken exp : " + exp);
				logger.debug("AuthorizationAspect checkToken new Date().getTime() : " + new Date().getTime());
				
				if(exp < new Date().getTime()) {
					throw new TokenExpiredException();
				}
				
			}
			
		} else {
			throw new InvalidTokenException();
		}
		
	}
	
}