package com.manman.common.exception;

public class AuthorizationHeaderNotExistsException extends RuntimeException {

	private static final long serialVersionUID = -1013888520992595369L;

	public AuthorizationHeaderNotExistsException() {
        super("Authorization 헤더가 없습니다.");
    }
	
}
