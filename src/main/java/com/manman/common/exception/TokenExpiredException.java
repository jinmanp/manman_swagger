package com.manman.common.exception;

public class TokenExpiredException extends RuntimeException {

	private static final long serialVersionUID = -3644870070470966659L;

	public TokenExpiredException() {
        super("토큰이 만료되었습니다.");
    }
	
}