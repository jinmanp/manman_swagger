package com.manman.common.exception;

public class InvalidTokenException extends RuntimeException {

	private static final long serialVersionUID = 346116252681253824L;

	public InvalidTokenException() {
        super("token 값이 유효하지 않습니다.");
    }
	
}