package com.manman.common.request;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter @Setter @ToString
@Schema(name = "TokenRequest")
public class TokenRequest {
	
	private String apiKey;
	
	@Schema(description = "현재시각(단위: millisecond)")
	private Long nonce;
	
}