package com.manman.common.web;

import java.nio.charset.StandardCharsets;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.crypto.SecretKey;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StreamUtils;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.manman.common.request.TokenRequest;
import com.manman.common.response.ErrorResponse;
import com.manman.common.response.TokenResponse;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.security.Keys;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.parameters.RequestBody;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.servlet.ServletInputStream;
import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@RequestMapping(value = "/token")
@Tag(name = "token", description = "Bearer JWT Token API")
@ConfigurationProperties(prefix = "manman.jwt.token")
@RequiredArgsConstructor
@RestController
public class TokenController {
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Setter 
	private String typ;
	@Setter 
	private String alg;
	@Setter 
	private String apiKey;
	@Setter 
	private String secretKey;
	
	// input 스트링으로 들어오는 String 데이터들의 white space를 trim해주는 역할을 한다.
	// 모든 요청이 들어올 때마다 해당 method를 거침 (node의 middleware 같은 것)
	@InitBinder
	public void initBinder(WebDataBinder dataBinder) {
		StringTrimmerEditor stringTrimmerEditor = new StringTrimmerEditor(true);
		dataBinder.registerCustomEditor(String.class, stringTrimmerEditor);
	}

	@Operation(description = "JWT token 생성")
	@Parameters({
		@Parameter(name = "apiKey", description = "apiKey", required = true),
		@Parameter(name = "nonce", description = "현재시각(단위: millisecond)", required = true)
	})
	@PostMapping(value = "")
	//@RequestMapping(value = "", method = {RequestMethod.POST})
	public ResponseEntity<? extends Object> createToken(
			@Parameter(hidden = true) @RequestBody TokenRequest tokenRequest
			, HttpServletRequest request
			) throws Exception {
		
		logger.debug("TokenController createToken request.getServerName() : " + request.getServerName());
		logger.debug("TokenController createToken request.getServerPort() : " + request.getServerPort());
		logger.debug("TokenController createToken request.getRemoteHost() : " + request.getRemoteHost());
		logger.debug("TokenController createToken request.getRemoteAddr() : " + request.getRemoteAddr());
		logger.debug("TokenController createToken request.getRemotePort() : " + request.getRemotePort());
		
		// 도메인 검증 추가!!
		// 개발일 때 ip 체크, 운영일 때 도메인 체크
		
		ServletInputStream inputStream = request.getInputStream();
        String requestBody = StreamUtils.copyToString(inputStream, StandardCharsets.UTF_8);
        logger.debug("TokenController callApiJsonRequest requestBody : " + requestBody);
		
		logger.debug("TokenController createToken tokenRequest.toString() : " + tokenRequest.toString());
		logger.debug("TokenController createToken tokenRequest.getApiKey() : " + tokenRequest.getApiKey());
		logger.debug("TokenController createToken tokenRequest.getNonce() : " + tokenRequest.getNonce());
		
		if(StringUtils.isBlank(tokenRequest.getApiKey()) || tokenRequest.getNonce() == null) {
			return ResponseEntity.badRequest().body(new ErrorResponse("400", "apiKey 또는 nonce가 없습니다.", null));
		} else if(!apiKey.equals(tokenRequest.getApiKey())) {
			return ResponseEntity.badRequest().body(new ErrorResponse("400", "apiKey가 일치하지 않습니다.", null));
		}

		Date now = new Date();
		long nonce = tokenRequest.getNonce();
		long expTime = 1800000L;	// 유효시간 : 30m
		
		logger.debug("TokenController createToken now.getTime() : " + now.getTime());
		logger.debug("TokenController createToken nonce : " + nonce);
		logger.debug("TokenController createToken new Date(nonce) : " + new Date(nonce));
		
		try {
			
			if(nonce < now.getTime() - expTime) {
				return ResponseEntity.badRequest().body(new ErrorResponse("400", "nonce값이 유효하지 않습니다.", null));
			}
			
		} catch(Exception e) {
			return ResponseEntity.badRequest().body(new ErrorResponse("400", "nonce값은 millisecond값으로 설정해야 합니다.", null));
		}

		SecretKey key = Keys.hmacShaKeyFor(secretKey.getBytes("UTF-8"));

		Map<String, Object> header = new HashMap<>();
		header.put("typ", typ);
		header.put("alg", alg);
		
		logger.debug("TokenController createToken new Date(nonce + expTime) : " + new Date(nonce + expTime));
		logger.debug("TokenController createToken new Date(nonce + expTime).getTime() : " + new Date(nonce + expTime).getTime());

		String jwt = Jwts.builder()
			.setHeader(header)
			.setIssuer("manmanApp")
			.setIssuedAt(now)
			.setExpiration(new Date(nonce + expTime))
			.claim("apiKey", apiKey)
			.signWith(key)
			.compact();
		
		return ResponseEntity.ok().body(new TokenResponse(jwt));
		
    }

}
