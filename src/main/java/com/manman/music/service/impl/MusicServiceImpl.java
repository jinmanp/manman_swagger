package com.manman.music.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.manman.mapper.manman.MusicMapper;
import com.manman.music.service.MusicService;
import com.manman.music.util.YoutubeCrawling;
import com.manman.music.vo.MusicRequest;
import com.manman.music.vo.MusicResponse;

/**
 * Music ServiceImpl 클래스
 * @author 박진만
 * @since 2018.04.19
 * @version 1.0.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *  수정일			수정자			수정내용
 *  -------    		--------    ---------------------------
 *  2018.04.19		박진만 		최초 생성
 * </pre>
 */
@Service("MusicService")
@Transactional(propagation=Propagation.SUPPORTS, readOnly=true)
public class MusicServiceImpl implements MusicService {
	
	final private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private MusicMapper musicMapper;

	/**
	 * Music 리스트 조회
	 */	
	@Override
	public List<MusicResponse> selectMusicList(MusicRequest musicRequest) throws Exception {
		
		List<MusicResponse> resultList = new ArrayList<MusicResponse>();
		
		//카테고리 별 Select query 분기
		if(musicRequest.getCategory().startsWith("R")) {
			resultList = musicMapper.selectRMusicList(musicRequest);
		}else if(musicRequest.getCategory().startsWith("D")) {
			
			if(null == musicRequest.getRegYmd() || "".equals(musicRequest.getRegYmd()) ) {
				resultList = musicMapper.selectD01MusicList(musicRequest);
			}else {
				resultList = musicMapper.selectD02MusicList(musicRequest);
			}
			
		}else if(musicRequest.getCategory().startsWith("Y")) {
			resultList = musicMapper.selectYMusicList(musicRequest);
		}
		
		return resultList;
	}

	/**
	 * Youtube 영상교체
	 */
	@Override
	public Boolean changeVideo(MusicRequest musicRequest) throws Exception {
		
		Boolean result = false;
		int updateResult = 0;
		
		//카테고리 별 Update query 분기
		if(musicRequest.getCategory().startsWith("R")) {
			updateResult = musicMapper.updateTop100Realtime(musicRequest);
		}else if(musicRequest.getCategory().startsWith("D")) {
			updateResult = musicMapper.updateTop100Day(musicRequest);			
		}else if(musicRequest.getCategory().startsWith("Y")) {
			updateResult = musicMapper.updateTop100Year(musicRequest);
		}
		
		if(updateResult == 1) {
			result = true;
		} else {
			result = false;
		}
		
		return result;
	}

	@Override
	public List<MusicResponse> selectSearchMusicList(MusicRequest musicRequest) throws Exception {
		
		YoutubeCrawling youtubeCrawling = new YoutubeCrawling();
		
		String searchWord = "";
		
		if("".equals(musicRequest.getArtist()) || null ==  musicRequest.getArtist()) {
			searchWord = musicRequest.getTitle();
		}else {
			searchWord = musicRequest.getTitle() + " " + musicRequest.getArtist();
		}
		
		List<MusicResponse> resultList = youtubeCrawling.searchList(searchWord, 1);
		
		return resultList;
		
	}
	
}
