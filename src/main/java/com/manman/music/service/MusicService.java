package com.manman.music.service;

import java.util.List;

import com.manman.music.vo.MusicRequest;
import com.manman.music.vo.MusicResponse;

/**
 * Music Servicel 클래스
 * @author 박진만
 * @since 2018.04.19
 * @version 1.0.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *  수정일			수정자			수정내용
 *  -------    		--------    ---------------------------
 *  2018.04.19		박진만 		최초 생성
 * </pre>
 */
public interface MusicService {	

	/**
	 * Music 리스트 조회
	 */
	List<MusicResponse> selectMusicList(MusicRequest musicRequest) throws Exception;

	/**
	 * Youtube 영상교체
	 */
	Boolean changeVideo(MusicRequest musicRequest) throws Exception;

	/**
	 * 검색
	 */
	List<MusicResponse> selectSearchMusicList(MusicRequest musicRequest) throws Exception;

}
