package com.manman.music.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class MusicResponse {
	
	@Schema(description = "등록번호")
	private String regNo;
	
	@Schema(description = "순위")
	private int rank;
	
	@Schema(description = "제목")
	private String title;
	
	@Schema(description = "가수")
	private String artist;
	
	@Schema(description = "등록일시")
	private String regYmd;
	
	@Schema(description = "카테고리")
	private String category;
	
	@Schema(description = "썸네일")
	private String thumbnail; //youtube thumbnail url
	
	@Schema(description = "비디오 ID")
	private String videoId; //youtube video key
	
	@Schema(description = "비디오 제목")
	private String videoTitle; //youtube video title
	
}
