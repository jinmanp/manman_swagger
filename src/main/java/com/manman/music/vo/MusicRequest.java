package com.manman.music.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MusicRequest {
	
	public interface validSelectMusicList {}
	public interface validSelectSearchMusicList {}
	public interface validChangeVideo {}
	public interface validSelectLyrics {}
	
	@NotBlank(groups = validSelectMusicList.class)
	@NotBlank(groups = validChangeVideo.class)
	@Schema(description = "카테고리")
    private String category;
	
    @Schema(description = "등록일시")
    private String regYmd;
    
    @Schema(description = "가수")
    private String artist;
    
    @NotBlank(groups = validSelectSearchMusicList.class)
    @Schema(description = "제목")
    private String title;
    
    @NotBlank(groups = validChangeVideo.class)
    @Schema(description = "등록번호")
    private String regNo;
    
    @NotBlank(groups = validChangeVideo.class)
    @Schema(description = "비디오 ID")
    private String videoId;
    
    @NotBlank(groups = validChangeVideo.class)
    @Schema(description = "썸네일")
    private String thumbnail;
    
    @Schema(description = "비디오 제목")
    public String videoTitle;
    
    @NotBlank(groups = validSelectLyrics.class)
    @Schema(description = "가사 검색 URL")
    private String searchResult;

}
