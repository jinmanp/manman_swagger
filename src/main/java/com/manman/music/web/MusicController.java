package com.manman.music.web;

import java.util.List;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.manman.common.response.ErrorResponse;
import com.manman.music.service.MusicService;
import com.manman.music.util.CrawlingLyrics;
import com.manman.music.vo.MusicRequest;
import com.manman.music.vo.MusicResponse;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.parameters.RequestBody;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;

@Tag(name = "Music", description = "Music api 입니다.")
@RestController
@RequestMapping("/music")
public class MusicController {
	
	final private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
    private MusicService musicService;
	
	@Autowired
    private CrawlingLyrics crawlingLyrics;
	
	@Operation(summary = "Music 리스트 조회", description = "Music 리스트 정보가 조회됩니다.")
    @ApiResponses({
            @ApiResponse(responseCode = "200", description = "OK",
                    content = @Content(schema = @Schema(implementation = MusicResponse.class))),
            @ApiResponse(responseCode = "400", description = "BAD REQUEST", 
            		content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = "404", description = "NOT FOUND",
            		content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = "500", description = "INTERNAL SERVER ERROR",
            		content = @Content(schema = @Schema(implementation = ErrorResponse.class)))
    })
	@Parameters({
		@Parameter(name = "category", description = "카테고리", example = "R01", required = true),
		@Parameter(name = "regYmd", description = "등록일시", example = "20230305", required = false)
	})
	@RequestMapping(value = "/selectMusicList", method = {RequestMethod.POST, RequestMethod.GET})
	ResponseEntity<List<MusicResponse>> selectMusicList(
			@Parameter(hidden = true) @Validated(MusicRequest.validSelectMusicList.class) @RequestBody MusicRequest musicRequest
    		) throws Exception {
		
		logger.debug("MusicController selectMusicList musicRequest.getCategory() : " + musicRequest.getCategory());
		logger.debug("MusicController selectMusicList musicRequest.getRegYmd() : " + musicRequest.getRegYmd());
		
		return ResponseEntity.ok().body(musicService.selectMusicList(musicRequest));
		
    }
	
	@Operation(summary = "Music 검색", description = "Music 검색 결과 목록이 조회됩니다.")
    @ApiResponses({
            @ApiResponse(responseCode = "200", description = "OK",
                    content = @Content(schema = @Schema(implementation = MusicResponse.class))),
            @ApiResponse(responseCode = "400", description = "BAD REQUEST", 
		    		content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
		    @ApiResponse(responseCode = "404", description = "NOT FOUND",
		    		content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
		    @ApiResponse(responseCode = "500", description = "INTERNAL SERVER ERROR",
		    		content = @Content(schema = @Schema(implementation = ErrorResponse.class)))
    })
	@Parameters({
		@Parameter(name = "artist", description = "가수", required = false),
		@Parameter(name = "title", description = "제목", required = true)
	})
	@RequestMapping(value = "/selectSearchMusicList", method = {RequestMethod.POST, RequestMethod.GET})
    ResponseEntity<List<MusicResponse>> selectSearchMusicList(
    		@Parameter(hidden = true) @Validated(MusicRequest.validSelectSearchMusicList.class) @RequestBody MusicRequest musicRequest
    		) throws Exception {
		
		logger.debug("MusicController selectSearchMusicList musicRequest.getArtist() : " + musicRequest.getArtist());
		logger.debug("MusicController selectSearchMusicList musicRequest.getTitle() : " + musicRequest.getTitle());
		
		return ResponseEntity.ok().body(musicService.selectSearchMusicList(musicRequest));
       
    }
	
	@Operation(summary = "Music 교체", description = "Music 데이터를 수정합니다.", hidden = true)
    @ApiResponses({
            @ApiResponse(responseCode = "200", description = "OK",
                    content = @Content(schema = @Schema(implementation = Boolean.class))),
            @ApiResponse(responseCode = "400", description = "BAD REQUEST", 
		    		content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
		    @ApiResponse(responseCode = "404", description = "NOT FOUND",
		    		content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
		    @ApiResponse(responseCode = "500", description = "INTERNAL SERVER ERROR",
		    		content = @Content(schema = @Schema(implementation = ErrorResponse.class)))
    })
	@Parameters({
		@Parameter(name = "category", description = "카테고리", example = "R01", required = true),
		@Parameter(name = "regNo", description = "등록번호", example = "2023022500R01001", required = true),
		@Parameter(name = "videoId", description = "비디오 ID", example = "Km71Rr9K-Bw", required = true),
		@Parameter(name = "thumbnail", description = "썸네일", example = "https://i.ytimg.com/vi/Km71Rr9K-Bw/default.jpg", required = true),
		@Parameter(name = "videoTitle", description = "비디오 제목")
	})
	@RequestMapping(value = "/changeVideo", method = {RequestMethod.POST})
    ResponseEntity<Boolean> changeVideo(
    		@Parameter(hidden = true) @Validated(MusicRequest.validChangeVideo.class) @RequestBody MusicRequest musicRequest
    		) throws Exception {
		
		logger.debug("MusicController changeVideo musicRequest.getCategory() : " + musicRequest.getCategory());
		logger.debug("MusicController changeVideo musicRequest.getRegNo() : " + musicRequest.getRegNo());
		logger.debug("MusicController changeVideo musicRequest.getVideoId() : " + musicRequest.getVideoId());
		logger.debug("MusicController changeVideo musicRequest.getThumbnail() : " + musicRequest.getThumbnail());
		logger.debug("MusicController changeVideo musicRequest.getVideoTitle() : " + musicRequest.getVideoTitle());
		
		Boolean result = musicService.changeVideo(musicRequest);
		
		return ResponseEntity.ok().body(result);
       
    }
	
	@Operation(summary = "Google cse 토큰 값 조회", description = "Google cse 토큰 정보가 조회됩니다.", hidden = true)
    @ApiResponses({
            @ApiResponse(responseCode = "200", description = "OK",
                    content = @Content(schema = @Schema(implementation = String.class))),
            @ApiResponse(responseCode = "400", description = "BAD REQUEST", 
            		content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = "404", description = "NOT FOUND",
            		content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = "500", description = "INTERNAL SERVER ERROR",
            		content = @Content(schema = @Schema(implementation = ErrorResponse.class)))
    })
	@RequestMapping(value = "/selectCseToc", method = {RequestMethod.POST, RequestMethod.GET})
    ResponseEntity<String> selectCseToc() throws Exception {
		
		Connection.Response response = Jsoup.connect("https://cse.google.com/cse.js?cx=007672770734803526218:21emw7atxz4").method(Connection.Method.GET).execute();
		Document document = response.parse();
		
		//logger.debug("selectCseToc document : " + document);
		
		String strDocument = document.toString();
		
		int idx = strDocument.indexOf("cse_token");
		
		String result = strDocument.substring(idx + "cse_token\": \"".length(),  strDocument.length()-1).split("\"")[0];
		
		//logger.debug("selectCseToc cseToken : " + result);
		
		return ResponseEntity.ok().body(result);
		
    }
	
	@Operation(summary = "가사 검색", description = "가사 검색 결과 정보가 조회됩니다.", hidden = false)
    @ApiResponses({
            @ApiResponse(responseCode = "200", description = "OK",
                    content = @Content(schema = @Schema(implementation = String.class))),
            @ApiResponse(responseCode = "400", description = "BAD REQUEST", 
		    		content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
		    @ApiResponse(responseCode = "404", description = "NOT FOUND",
		    		content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
		    @ApiResponse(responseCode = "500", description = "INTERNAL SERVER ERROR",
		    		content = @Content(schema = @Schema(implementation = ErrorResponse.class)))
    })
	@Parameters({
		@Parameter(name = "searchResult", description = "가사 검색 URL", required = true)
	})
	@RequestMapping(value = "/selectLyrics", method = {RequestMethod.POST, RequestMethod.GET})
    ResponseEntity<String> selectLyrics(
    		@Parameter(hidden = true) @Validated(MusicRequest.validSelectLyrics.class) @RequestBody MusicRequest musicRequest
    		) throws Exception {
		
		logger.debug("MusicController selectLyrics searchResult : " + musicRequest.getSearchResult());
		
		String result = crawlingLyrics.getLyrics(musicRequest.getSearchResult());
		
		return ResponseEntity.ok().body(result);
       
    }	
	
}
